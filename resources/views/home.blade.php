@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(Auth::user()->is_admin == 0)
                <div class="card-header">{{ __('Dashboard') }}
                </div>
                @elseif(Auth::user()->is_admin == 1)
                <div class="card-header">{{ __('Dashboard') }}
                    <p align="right"><a href="{{route('admin.users.index')}}">Approve User</a></p>
                    <p align="right"><a href="{{route('admin.users.check')}}">History Login Activity User</a></p>
                </div>
                @endif
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
