@extends('layouts.app')
<!DOCTYPE html>
<html>
<head>
	<title>Daftar Device</title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
</head>
@section('content')
<body>
	<div class="container">
		<div class="card mt-5">
			<div class="card-body">
				
				<h3 class="text-center">Daftar History Login Activity User</h3>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th class="text-center">Nama User</th>
                            <th class="text-center">Last Login</th>
                            <th class="text-center">Ip</th>
						</tr>
					</thead>
					<tbody>
                        @foreach($users as $user)
						<tr>
							<td class="text-center">{{$user->name}}</td>
                            <td class="text-center">{{$user->last_login_time}}</td>
                            <td class="text-center">{{$user->last_login_ip}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
@endsection
</html>