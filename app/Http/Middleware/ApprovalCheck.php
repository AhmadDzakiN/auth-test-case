<?php

namespace App\Http\Middleware;

use Closure;

class ApprovalCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->is_approve) {
            return redirect()->route('approve');
        }

        return $next($request);
    }
}
