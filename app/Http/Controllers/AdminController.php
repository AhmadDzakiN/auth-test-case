<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::whereNull('is_approve')->get();

        return view('admin', compact('users'));
    }

    public function approve($user_id)
    {
        $user = User::findOrFail($user_id);
        $user->update(['is_approve' => 1]);

        return redirect()->route('admin.users.index')->withMessage('User approved successfully');
    }
    public function checkActivity(){
        $users = User::whereNotNull('last_login_time')->get();
        // dd($users);

        return view('checkActivity', compact('users'));
    }
}
